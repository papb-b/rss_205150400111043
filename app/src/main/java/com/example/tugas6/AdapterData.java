package com.example.tugas6;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData> {
    List<ArrayList<String>> listData;
    LayoutInflater inflater;
    Context context;


    public AdapterData(Context context, List<ArrayList<String>> listData) {
        this.listData = listData;
        this.inflater= LayoutInflater.from(context);
        this.context=context;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item, parent, false);
        return new HolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        holder.text.setText(listData.get(position).get(0));
        holder.ll.setOnClickListener(v->{
            Intent it = new Intent();
            it.setAction(Intent.ACTION_VIEW);
            it.addCategory(Intent.CATEGORY_BROWSABLE);
            it.setData(Uri.parse(listData.get(position).get(1)));
            this.context.startActivity(it);
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
    public class HolderData extends RecyclerView.ViewHolder{
        TextView text;
        TextView text2;
        TextView text3;
        ImageView img;
        LinearLayout ll;

        public HolderData(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.title);
            text2 = itemView.findViewById(R.id.txtContent);
            text3 = itemView.findViewById(R.id.pubDate);
//            img = itemView.findViewById(R.id.img);
            ll = itemView.findViewById(R.id.parentlayout);
        }
    }
}
